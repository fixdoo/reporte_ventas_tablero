from odoo import api, fields, models, tools
from odoo.tools.safe_eval import pytz


class SaleOfmonth(models.Model):
    _name = 'sale.month'
    _auto = False
    _order = "month desc"

    amount_total = fields.Float(string='Total', group_operator="sum")
    currency_id = fields.Many2one('res.currency', string='Moneda')
    company_id = fields.Many2one('res.company', )
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
    ], string='Status', )
    month = fields.Datetime(string='Mes', )
    amount_goal = fields.Float(string="Meta", compute="_compute_amount_meta")
    mes = fields.Char(string="Mes", compute="_compute_mes_periodo")

    def _compute_mes_periodo(self):
        for record in self:
            record.mes = f"{record.month.month}/{record.month.year}"

    def _compute_amount_meta(self):
        for record in self:
            goal = self.env["sale.goals.line"].search([
                ("company_id", "=", record.company_id.id),
                ("date_start", "<=", record.month),
                ("date_stop", ">=", record.month)
            ], limit=1)
            if goal:
                record.amount_goal = goal.amount
            else:
                record.amount_goal = 0

    def _get_main_request(self):
        timezone = self.env.user.tz or 'UTC'
        request = """
            CREATE or REPLACE VIEW %s AS (
                SELECT 
                    row_number() OVER (PARTITION BY true) as id,
                    sum(so.amount_total) AS amount_total,
                    so.currency_id AS currency_id,
                    so.company_id AS company_id,
                    so.state AS state,
                    DATE_TRUNC('month', so.date_order) AS  month
                FROM
                    sale_order AS so
                    WHERE so.state = 'sale'
                GROUP BY
                    so.currency_id,
                    so.company_id,
                    so.state,
                    month
            )
        """ % (self._table)
        return request

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(self._get_main_request())
