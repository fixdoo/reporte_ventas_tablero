from odoo import api, fields, models


class SaleAVGMonth(models.Model):
    _name = 'sale.avg.month'

    amount_avg = fields.Float(string='Promedio x mes')
    amount_total = fields.Float(string='Venta total x mes')
    total_orders = fields.Integer(string='Ordenes totales en mes')
    month = fields.Char(string='Mes', )
    tablero_report_id = fields.Many2one("sale.tablero.report")


class BestSaleMonth(models.Model):
    _name = 'best.sale.month'
    _description = "Reporte top 10 ventas"

    order_id = fields.Many2one("sale.order", string="Venta")
    lugar = fields.Integer(string='Lugar')
    amount_total = fields.Float("Monto total")
    date = fields.Datetime(related="order_id.date_order", string="Fecha orden")
    user_id = fields.Many2one("res.users", related="order_id.user_id", string="Vendedor")
    tablero_report_id = fields.Many2one("sale.tablero.report", )


class SaleTeamPercentage(models.Model):
    _name = 'sale.team.percentage'
    _description = 'Reporte porcentaje por equipo de ventas'

    percentage = fields.Float(string='Porcentaje')
    total_sales = fields.Float("No. ventas totales")
    total_team = fields.Float("No. de ventas")
    team_id = fields.Many2one('crm.team', 'Equipo de ventas', )
    tablero_report_id = fields.Many2one("sale.tablero.report", )


class SalePaymentPending(models.Model):
    _name = 'sale.payment.pending'
    _description = 'Ventas pendientes de pago'

    order_id = fields.Many2one("sale.order", string="Orden")
    amount_total = fields.Float("Monto")
    tablero_report_id = fields.Many2one("sale.tablero.report", )


class SaleSeller(models.Model):
    _name = 'sale.seller'
    _description = 'Reporte ventas por vendedor'

    user_id = fields.Many2one('res.users', string='Vendedor',)
    amount_total = fields.Float("Monto total ventas")
    porcent_total = fields.Float("Porcentaje de ventas total ventas")
    total_sales = fields.Float("No. ventas totales")
    tablero_report_id = fields.Many2one("sale.tablero.report", )
    promedio_de_venta = fields.Float("Promedio venta")


class SaleMonthReport(models.Model):
    _name = 'sale.month.report'
    _description = "Mes vs meta"

    amount_total = fields.Float(string='Total', group_operator="sum")
    amount_goal = fields.Float(string="Meta", compute="_compute_amount_meta", store=True)
    mes = fields.Char(string="Mes", compute="_compute_mes", store=True)
    date_order = fields.Datetime(string="fecha orden")
    tablero_report_id = fields.Many2one("sale.tablero.report", )

    @api.depends("tablero_report_id", "amount_total", "date_order", "tablero_report_id.cron_update")
    def _compute_amount_meta(self):
        for record in self:
            goals = self.env["sale.goals.line"].search([
                ("company_id", "=", record.env.user.company_id.id),
                ("date_start", "<=", record.date_order),
                ("date_stop", ">=", record.date_order)
            ])
            if goals:
                record.amount_goal = sum(goals.mapped("amount"))
            else:
                record.amount_goal = 0

    @api.depends("tablero_report_id", "amount_total", "date_order", "tablero_report_id.cron_update")
    def _compute_mes(self):
        for record in self:
            if record.date_order:
                record.mes = f"{record.date_order.month}/{record.date_order.year}"
            else:
                record.mes = ""


class SaleMonthReportUser(models.Model):
    _name = 'sale.month.report.user'
    _description = "Mes vs meta vendedores"

    amount_total = fields.Float(string='Total', group_operator="sum")
    amount_goal = fields.Float(string="Meta", compute="_compute_amount_meta", store=True)
    mes = fields.Char(string="Mes", compute="_compute_mes", store=True)
    date_order = fields.Datetime(string="fecha orden")
    tablero_report_id = fields.Many2one("sale.tablero.report", )
    user_id = fields.Many2one("res.users", string="Vendedor")

    @api.depends("tablero_report_id", "user_id", "tablero_report_id.cron_update")
    def _compute_amount_meta(self):
        for record in self:
            goal = self.env["sale.goals.line"].search([
                ("company_id", "=", record.env.user.company_id.id),
                ("name", "=", record.mes),
                ("user_id", "=", record.user_id.id)
            ], limit=1)
            if goal:
                record.amount_goal = goal.amount
            else:
                record.amount_goal = 0

    @api.depends("tablero_report_id", "user_id", "tablero_report_id.cron_update")
    def _compute_mes(self):
        for record in self:
            if record.date_order:
                record.mes = f"{record.date_order.month}/{record.date_order.year}"
            else:
                record.mes = ""


class SaleStates(models.Model):
    _name = 'sale.states'
    _description = 'Reporte ventas pr estado'

    amount_total = fields.Float(string='Venta total')
    total_orders = fields.Integer(string='Ordenes totales')
    tablero_report_id = fields.Many2one("sale.tablero.report")
    state_id = fields.Many2one("res.country.state", string='Estado')


class ReportHelpDeskTicket(models.Model):
    _name = 'report.helpdesk.ticket.stage'
    _description = "Reporte tickets por etapa"

    stage_id = fields.Many2one("helpdesk.stage", string="Estado")
    total_tickets = fields.Integer(string='Tickets totales')
    tablero_report_id = fields.Many2one("sale.tablero.report")


class ReportHelpDeskMonth(models.Model):
    _name = 'report.helpdesk.ticket.month'
    _description = "Reporte tickets por mes"

    total_tickets = fields.Float(string='Total', group_operator="sum")
    mes = fields.Char(string="Mes", compute="_compute_mes", store=True)
    assign_date = fields.Datetime(string="Fecha")
    tablero_report_id = fields.Many2one("sale.tablero.report", )

    @api.depends("tablero_report_id", "total_tickets", "assign_date", "tablero_report_id.cron_update")
    def _compute_mes(self):
        for record in self:
            if record.assign_date:
                record.mes = f"{record.assign_date.month}/{record.assign_date.year}"
            else:
                record.mes = ""


class ReportHelpDeskTicket(models.Model):
    _name = 'report.helpdesk.ticket.stage.user'
    _description = "Reporte tickets por etapa x usuario"

    stage_id = fields.Many2one("helpdesk.stage", string="Estado")
    total_tickets = fields.Integer(string='Tickets totales')
    tablero_report_id = fields.Many2one("sale.tablero.report")
    user_id = fields.Many2one("res.users", string="Vendedor")


class ReportHelpDeskMonth(models.Model):
    _name = 'report.helpdesk.ticket.month.user'
    _description = "Reporte tickets por mes x usuario"

    total_tickets = fields.Float(string='Total', group_operator="sum")
    mes = fields.Char(string="Mes", compute="_compute_mes", store=True)
    assign_date = fields.Datetime(string="Fecha")
    tablero_report_id = fields.Many2one("sale.tablero.report", )
    user_id = fields.Many2one("res.users", string="Vendedor")

    @api.depends("tablero_report_id", "total_tickets", "assign_date", "tablero_report_id.cron_update")
    def _compute_mes(self):
        for record in self:
            if record.assign_date:
                record.mes = f"{record.assign_date.month}/{record.assign_date.year}"
            else:
                record.mes = ""

    class ReportHelpDeskMonth(models.Model):
        _name = 'report.rating.rating'
        _description = "Reporte tickets rating"

        total_tickets = fields.Float(string='Total', group_operator="sum")
        tablero_report_id = fields.Many2one("sale.tablero.report", )
        rating_text = fields.Char(string="Calificacion")
        percentage = fields.Float(string='Porcentaje')
