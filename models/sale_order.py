from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    state_id = fields.Many2one(
        "res.country.state", string='Estado', related="partner_id.state_id", store=True, readonly=True)