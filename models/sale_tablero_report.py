from odoo import api, fields, models, tools
from odoo.exceptions import ValidationError


class SaletableroReport(models.Model):
    _name = 'sale.tablero.report'

    name = fields.Char(string="Nombre reporte")
    active = fields.Boolean(string="Activo", default=True)
    date_start = fields.Date(string="Fecha incio")
    date_stop = fields.Date(string="Fecha fin")
    meta_anual = fields.Float(string="Meta anual", compute="_compute_meta_real_anual", store=True)
    real_anual = fields.Float(string="Total anual", compute="_compute_meta_real_anual", store=True)
    team_ids = fields.Many2many('crm.team', string='Equipo de ventas')
    cron_update = fields.Boolean(string="Se actualiza")
    total_tickets_periodo = fields.Float(
        string="Total de tickets atendidos",
        compute="_compute_total_tickets_periodo",
        store=True
    )
    venta_promedio = fields.Float(
        string="Monto promedio de venta",
        compute="_compute_venta_promedio_periodo",
        store=True
    )
    promedio_mes = fields.One2many(
        comodel_name="sale.avg.month",
        inverse_name="tablero_report_id",
        string="Promedio x Mes",
        compute="_compute_obtener_ventas_promedio_periodo_mes", store=True
    )
    sales_pendientes_payment = fields.One2many(
        "sale.payment.pending",
        inverse_name="tablero_report_id",
        string="Ventas pendientes de pago",
        compute="_compute_sales_pendientes_payment", store=True
    )
    best_sales = fields.One2many(
        "best.sale.month",
        inverse_name="tablero_report_id",
        string="Top 10 ventas del mes",
        compute="_compute_obtener_10_mejores_mes", store=True
    )
    porcentaje_equipo_ventas = fields.One2many(
        "sale.team.percentage",
        inverse_name="tablero_report_id",
        string="Ventas equipo porcentaje",
        compute="_compute_porcentaje_equipo_ventas", store=True
    )
    ventas_vs_metas = fields.One2many(
        "sale.month.report",
        inverse_name="tablero_report_id",
        string="Ventas vs meta x mes",
        compute="_compute_ventas_vs_metas", store=True
    )
    ventas_vs_metas_vendedor = fields.One2many(
        "sale.month.report.user",
        inverse_name="tablero_report_id",
        string="Ventas vs meta x mes vendedor",
        compute="_compute_ventas_vs_metas_vendedor", store=True
    )
    total_saldo_pendiene = fields.Float(string="Total Saldo pendiendte", compute="_compute_sales_pendientes_payment", store=True)

    ventas_x_vendedor = fields.One2many(
        "sale.seller",
        inverse_name="tablero_report_id",
        string="Ventas x Vendedor",
        compute="_compute_ventas_x_vendedor", store=True
    )
    ventas_por_estado = fields.One2many(
        "sale.states",
        inverse_name="tablero_report_id",
        string="Ventas x estado",
        compute="_compute_ventas_por_estado", store=True
    )
    tickets_por_etapa = fields.One2many(
        "report.helpdesk.ticket.stage",
        inverse_name="tablero_report_id",
        string="Tickets x estado",
        compute="_compute_tickets_por_etapa", store=True
    )
    tickets_por_mes = fields.One2many(
        "report.helpdesk.ticket.month",
        inverse_name="tablero_report_id",
        string="Tickets x estado",
        compute="_compute_tickets_por_mes", store=True
    )
    tickets_por_etapa_usuario = fields.One2many(
        "report.helpdesk.ticket.stage.user",
        inverse_name="tablero_report_id",
        string="Tickets x estado",
        compute="_compute_tickets_por_etapa_usuario", store=True
    )
    tickets_por_mes_usuario = fields.One2many(
        "report.helpdesk.ticket.month.user",
        inverse_name="tablero_report_id",
        string="Tickets x estado usuario",
        compute="_compute_tickets_por_mes_usuario", store=True
    )
    rating_report = fields.One2many(
        "report.rating.rating",
        inverse_name="tablero_report_id",
        string="Satisfaccion",
        compute="_compute_rating_report", store=True
    )
    order_ids = fields.Many2many(comodel_name="sale.order", string="ordenes", compute="_compute_sale_order", store=True)
    tickets_ids = fields.Many2many(comodel_name="helpdesk.ticket", string="Tickets", compute="_compute_tickets", store=True)

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_tickets(self):
        for record in self:
            ticket_ids = record.env["helpdesk.ticket"].search([
                ("assign_date", "<=", record.date_stop),
                ("assign_date", ">=", record.date_start),
            ])
            record.update({
                "tickets_ids": [(6, 0, ticket_ids.ids)]
            })

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_sale_order(self):
        for record in self:
            order_ids = record.env["sale.order"].search([
                ("date_order", "<=", record.date_stop),
                ("date_order", ">=", record.date_start),
                ("state", "=", "sale")
            ])
            record.update({
                "order_ids": [(6, 0, order_ids.ids)]
            })

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_rating_report(self):
        for record in self:
            record.write({
                "rating_report": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                rating_ids = record.env["rating.rating"].search([
                    ("create_date", "<=", record.date_stop),
                    ("create_date", ">=", record.date_start)
                ])
                total_ratings = len(rating_ids)
                text_ratings = rating_ids.mapped("rating_text")
                for text_rating in text_ratings:
                    for_rating = rating_ids.filtered(lambda x: x.rating_text == text_rating)
                    total_rating = len(for_rating)
                    porcentaje = (total_rating/total_ratings)*100 if total_ratings > 0 else 0.0
                    values = {
                        "total_tickets": total_rating,
                        "rating_text": text_rating,
                        "percentage": porcentaje,
                    }
                    vals.append((0, 0, values))
                record.write({
                    "rating_report": vals
                })

    @api.depends("tickets_ids", "cron_update")
    def _compute_tickets_por_mes_usuario(self):
        for record in self:
            record.write({
                "tickets_por_mes_usuario": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                ticket_ids = record.tickets_ids
                vendedores = ticket_ids.mapped("user_id")
                for vendedor in vendedores:
                    for i in range(1, 13):
                        by_month = ticket_ids.filtered(lambda x: x.assign_date.month == i and x.user_id == vendedor)
                        values = {
                            "total_tickets": len(by_month),
                            "assign_date": fields.datetime(int(record.date_start.year), int(i), 1, 18, 30, 0),
                            "user_id": vendedor.id
                        }
                        vals.append((0, 0, values))
                record.write({
                    "tickets_por_mes_usuario": vals
                })

    @api.depends("tickets_ids", "cron_update")
    def _compute_tickets_por_etapa_usuario(self):
        for record in self:
            record.write({
                "tickets_por_etapa_usuario": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                ticket_ids = record.tickets_ids
                etapas = ticket_ids.mapped("stage_id")
                vendedores = ticket_ids.mapped("user_id")
                for vendedor in vendedores:
                    for etapa in etapas:
                        por_etapa = ticket_ids.filtered(lambda x: x.stage_id == etapa and x.user_id == vendedor)
                        values = {
                            "total_tickets": len(por_etapa),
                            "stage_id": etapa.id,
                            "user_id": vendedor.id
                        }
                        vals.append((0, 0, values))
                record.write({
                    "tickets_por_etapa_usuario": vals
                })

    @api.depends("tickets_ids", "cron_update")
    def _compute_tickets_por_mes(self):
        for record in self:
            record.write({
                "tickets_por_mes": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                ticket_ids = record.tickets_ids
                for i in range(1, 13):
                    by_month = ticket_ids.filtered(lambda x: x.assign_date.month == i)
                    values = {
                        "total_tickets": len(by_month),
                        "assign_date": fields.datetime(int(record.date_start.year), int(i), 1, 18, 30, 0)
                    }
                    vals.append((0, 0, values))
                record.write({
                    "tickets_por_mes": vals
                })

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_total_tickets_periodo(self):
        for record in self:
            if record.date_start and record.date_stop:
                total_tickets = record.env["helpdesk.ticket"].search_count([
                    ("assign_date", "<=", record.date_stop),
                    ("assign_date", ">=", record.date_start),
                    ("stage_id.name", "=", "Hecho")
                ])
                record.total_tickets_periodo = total_tickets
            else:
                record.total_tickets_periodo = 0

    @api.depends("tickets_ids", "cron_update")
    def _compute_tickets_por_etapa(self):
        for record in self:
            record.write({
                "tickets_por_etapa": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                ticket_ids = record.tickets_ids
                etapas = ticket_ids.mapped("stage_id")
                for etapa in etapas:
                    por_etapa = ticket_ids.filtered(lambda x: x.stage_id == etapa)
                    values = {
                        "total_tickets": len(por_etapa),
                        "stage_id": etapa.id,
                    }
                    vals.append((0, 0, values))
                record.write({
                    "tickets_por_etapa": vals
                })

    @api.depends("order_ids", "cron_update")
    def _compute_ventas_por_estado(self):
        for record in self:
            record.write({
                "ventas_por_estado": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                order_ids = record.order_ids
                estados = order_ids.mapped("state_id")
                for estado in estados:
                    por_estado = order_ids.filtered(lambda x: x.state_id == estado)
                    values = {
                        "amount_total": sum(por_estado.mapped("amount_total")),
                        "total_orders": len(por_estado),
                        "state_id": estado.id
                    }
                    vals.append((0, 0, values))
                record.write({
                    "ventas_por_estado": vals
                })

    @api.depends("order_ids", "cron_update")
    def _compute_ventas_vs_metas_vendedor(self):
        for record in self:
            #record.ventas_vs_metas_vendedor.unlink()
            record.write({
                "ventas_vs_metas_vendedor": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                order_ids = record.order_ids
                vendedores = order_ids.mapped("user_id")
                for vendedor in vendedores:
                    for i in range(1, 13):
                        by_month = order_ids.filtered(lambda x: x.date_order.month == i and x.user_id.id == vendedor.id)
                        values = {
                            "amount_total": sum(by_month.mapped("amount_total")),
                            "date_order": fields.datetime(int(record.date_start.year), int(i), 1, 18, 30, 0),
                            "user_id": vendedor.id
                        }
                        vals.append((0, 0, values))
                record.write({
                    "ventas_vs_metas_vendedor": vals
                })

    @api.depends("order_ids", "cron_update")
    def _compute_ventas_vs_metas(self):
        for record in self:
            record.write({
                "ventas_vs_metas": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                order_ids = record.order_ids
                for i in range(1, 13):
                    by_month = order_ids.filtered(lambda x: x.date_order.month == i)
                    values = {
                        "amount_total": sum(by_month.mapped("amount_total")),
                        "date_order": fields.datetime(int(record.date_start.year), int(i), 1, 18, 30, 0)
                    }
                    vals.append((0, 0, values))
                record.write({
                    "ventas_vs_metas": vals
                })

    @api.constrains("active")
    def constraint_active(self):
        for record in self:
            reports = record.search([("active", "=", True)])
            if len(reports) > 1:
                raise ValidationError("Solo se permite tener un reporte activo")

    @api.depends("order_ids", "cron_update")
    def _compute_ventas_x_vendedor(self):
        for record in self:
            record.write({
                "ventas_x_vendedor": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                vals = []
                order_ids = record.order_ids
                total_venta_periodo = len(order_ids)
                vendedores = order_ids.mapped("user_id")
                for vendedor in vendedores:
                    sale_seller = order_ids.filtered(lambda x: x.user_id.id == vendedor.id)
                    total_ventas = len(sale_seller)
                    amount_total_vendedor = sum(sale_seller.mapped('amount_total'))
                    promedio = amount_total_vendedor/total_ventas if total_ventas > 0 else 0.0
                    porcentaje = (total_ventas/total_venta_periodo)*100 if total_venta_periodo > 0 else 0.0
                    values = {
                        "total_sales": total_ventas,
                        "porcent_total": porcentaje,
                        "amount_total": amount_total_vendedor,
                        "user_id": vendedor.id,
                        "promedio_de_venta": promedio,
                    }
                    vals.append((0, 0, values))
                record.write({
                    "ventas_x_vendedor": vals
                })


    @api.depends("order_ids", "cron_update")
    def _compute_meta_real_anual(self):
        for record in self:
            if record.date_start and record.date_stop:
                order_ids = record.order_ids
                total_goal = 0.0
                goals_ids = record.env["sale.goals.line"].search([
                    ("date_start", ">=", record.date_start),
                    ("date_stop", "<=", record.date_stop)
                ])
                for goal in goals_ids:
                    total_goal += goal.amount
                record.real_anual = sum(order_ids.mapped("amount_total"))
                record.meta_anual = total_goal

    @api.depends("order_ids", "cron_update")
    def _compute_sales_pendientes_payment(self):
        for record in self:
            record.write({
                "sales_pendientes_payment": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                order_ids = record.order_ids
                values = []
                total = 0.0
                for order in order_ids:
                    invoices = order.order_line.invoice_lines.move_id.filtered(
                        lambda r: r.move_type in ('out_invoice', 'out_refund') and
                                  r.payment_state in ('partial', 'in_payment', 'not_paid'))
                    if invoices:
                        amount = sum(invoices.mapped("amount_residual"))
                        total += amount
                        vals={
                            "amount_total": amount,
                            "order_id": order.id
                        }
                        values.append((0, 0, vals))
                record.write({
                    "sales_pendientes_payment": values
                })
                record.total_saldo_pendiene = total

    @api.depends("order_ids", "team_ids", "cron_update")
    def _compute_porcentaje_equipo_ventas(self):
        for record in self:
            record.write({
                "porcentaje_equipo_ventas": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop and record.team_ids:
                vals = []
                order_ids = record.order_ids
                total_sales = len(order_ids)
                for team in record.team_ids:
                    order_teams = order_ids.filtered(lambda x: x.team_id == team)
                    total_team = len(order_teams)
                    values = {
                        "team_id": team.ids[0],
                        "total_sales": total_sales,
                        "total_team": total_team,
                        "percentage": (total_team/total_sales) * 100 if total_sales else 0.0,
                    }
                    vals.append((0, 0, values))
                record.write({
                    "porcentaje_equipo_ventas": vals
                })

    @api.depends("order_ids", "cron_update")
    def _compute_venta_promedio_periodo(self):
        for record in self:
            if record.date_start and record.date_stop:
                order_ids = record.order_ids
                num_ordenes = len(order_ids)
                amount_total = sum(order_ids.mapped("amount_total"))
                record.venta_promedio = amount_total/num_ordenes if num_ordenes > 0 else 0.0
            else:
                record.venta_promedio = 0

    def _get_sql_consult_10_mejores(self):
        request = """
            SELECT 
                id,
                amount_total
            FROM
                sale_order
                WHERE '%s' <= date_order and  date_order<= '%s' and state = 'sale'
            ORDER BY amount_total DESC
            LIMIT 10
            """ % (self.date_start, self.date_stop)
        return request

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_obtener_10_mejores_mes(self):
        for record in self:
            record.write({
                "best_sales": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                record.env.cr.execute(record._get_sql_consult_10_mejores())
                vals = []
                count = 1
                for rec in record._cr.dictfetchall():
                    amount_total = rec.get('amount_total')
                    order_id = rec.get('id')
                    values = {
                        "order_id": order_id,
                        "amount_total": amount_total,
                        "lugar": count,
                    }
                    vals.append((0, 0, values))
                    count += 1
                record.write({
                    "best_sales": vals
                })

    def _consulta_ventas_prom_periodo(self):
        request = """
            SELECT 
                count(id) as total_orders,
                sum(so.amount_total) AS amount_total,
                so.company_id AS company_id,
                DATE_TRUNC('month', so.date_order) AS  month
            FROM
                sale_order AS so
                WHERE extract(year from so.date_order) = %s and state = 'sale'
            GROUP BY
                so.company_id,
                month
            """ % (fields.datetime.now().year)
        return request

    @api.depends("date_start", "date_stop", "cron_update")
    def _compute_obtener_ventas_promedio_periodo_mes(self):
        for record in self:
            record.write({
                "promedio_mes": [(5, 0, 0)]
            })
            if record.date_start and record.date_stop:
                record.env.cr.execute(record._consulta_ventas_prom_periodo())
                vals = []
                for rec in record._cr.dictfetchall():
                    mes = f'{rec.get("month").month}/{rec.get("month").year}'
                    promedio = rec.get('amount_total') / rec.get('total_orders')
                    amount_total = rec.get('amount_total')
                    total_orders = rec.get('total_orders')
                    values = {
                        "month": mes,
                        "amount_avg": promedio,
                        "amount_total": amount_total,
                        "total_orders": total_orders,
                    }
                    vals.append((0, 0, values))
                record.write({
                    "promedio_mes": vals
                })

    def _update_report_cron(self):
        avg = self.env['sale.avg.month'].search([])
        avg.unlink()
        avg = self.env['sale.seller'].search([])
        avg.unlink()
        avg = self.env['sale.month.report.user'].search([])
        avg.unlink()
        avg = self.env['sale.month.report'].search([])
        avg.unlink()
        avg = self.env['best.sale.month'].search([])
        avg.unlink()
        avg = self.env['sale.team.percentage'].search([])
        avg.unlink()
        avg = self.env['sale.payment.pending'].search([])
        avg.unlink()
        avg = self.env['sale.states'].search([])
        avg.unlink()
        avg = self.env['report.helpdesk.ticket.stage'].search([])
        avg.unlink()
        avg = self.env['report.helpdesk.ticket.month'].search([])
        avg.unlink()
        avg = self.env['report.helpdesk.ticket.stage.user'].search([])
        avg.unlink()
        avg = self.env['report.helpdesk.ticket.month.user'].search([])
        avg.unlink()
        reports = self.search([])
        self.env.cr.commit()
        for report in reports:
            if report.cron_update:
                report.cron_update = False
            else:
                report.cron_update = True
