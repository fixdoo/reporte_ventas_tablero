from odoo import api, fields, models
import calendar, datetime


class SaleGoals(models.Model):
    _name = 'sale.goals'

    name = fields.Char(string="Nombre", )
    year_id = fields.Many2one("date.year", string="Año de meta", )
    amount = fields.Float(string="Meta", compute="_compute_amount", store=True, readonly=False)
    state = fields.Selection(
        selection=[
            ("draft","En proceso"),
            ("done", "Cerrado")], string="Estado", default="draft")
    company_id = fields.Many2one('res.company', string="Compañia", default=lambda s: s.env.company, required=True)
    user_id = fields.Many2one('res.users', string='Vendedor', )
    goal_line_ids = fields.One2many(
        "sale.goals.line",
        inverse_name="goal_id",
        string="tiempo",
    )

    @api.depends("goal_line_ids.amount")
    def _compute_amount(self):
        for record in self:
            total = sum(record.goal_line_ids.mapped("amount"))
            record.amount = total

    def create_period_goal(self):
        self.goal_line_ids.unlink()
        vals = []
        for i in range(1, 13):
            days = calendar.monthrange(int(self.year_id.name), i)
            value = {
                "name": f'{i}/{self.year_id.code}',
                "date_start": datetime.date(int(self.year_id.code), i, 1),
                "date_stop": datetime.date(int(self.year_id.code), i, days[1])
            }
            vals.append((0, 0, value))
        self.write({
            "goal_line_ids": vals
        })


class SaleGoalsLine(models.Model):
    _name = 'sale.goals.line'

    name = fields.Char(string="Periodo", )
    year_id = fields.Many2one("date.year", string="Año de meta", related="goal_id.year_id")
    date_start = fields.Date(string="Fecha incio")
    date_stop = fields.Date(string="Fecha fin")
    amount = fields.Float(string="Meta", )
    company_id = fields.Many2one('res.company', string="Compañia", default=lambda s: s.env.company, required=True)
    user_id = fields.Many2one('res.users', string='Vendedor', related="goal_id.user_id")
    goal_id = fields.Many2one("sale.goals")
